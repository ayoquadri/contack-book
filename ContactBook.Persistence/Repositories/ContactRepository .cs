﻿ using System.Data.Entity;
 using System.Linq;
 using ContackBook.Model;
using ContackBook.Model.Persistence.Repositories;

namespace ContactBook.Persistence.Repositories
{
    public class ContactRepository : AbstractRepository<Contact, ContackBookDbContext>, IContactRepository
    {
        //Contact Specific Db Queries goes here
        public IQueryable<Contact> GetContacts()
        {
            return DataContext.Set<Contact>();
        }
    }
}
