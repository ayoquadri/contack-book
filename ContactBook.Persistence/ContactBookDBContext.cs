﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Data.Entity;
using System.Reflection;
using System.Threading.Tasks;
using ContackBook.Model;
using ContactBook.Persistence.Configurations;
using System.ComponentModel.Composition;

namespace ContactBook.Persistence
{
    public class ContackBookDbContext : DbContext
    {
        public ContackBookDbContext()
            : base("DefaultConnection")
        {

        }
        public static ContackBookDbContext Create()
        {
            return new ContackBookDbContext();
        }
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //GEt all configuration and mapping in the assembly
            var contextConfiguration = new ContextConfiguration();
            var catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
            var container = new CompositionContainer(catalog);

            container.ComposeParts(contextConfiguration);

            foreach (var configuration in contextConfiguration.Configurations)
            {
                configuration.AddConfiguration(modelBuilder.Configurations);
            }

            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync()
        {
            DoSaveChanges();
            return base.SaveChangesAsync();
        }
        public override int SaveChanges()
        {
            DoSaveChanges();
            return base.SaveChanges();
        }

        private void DoSaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries<BaseEntity>())
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Entity.DateCreated = entry.Entity.DateModified = DateTime.Now;
                }
                else if (entry.State == EntityState.Modified)
                {
                    entry.Property(e => e.DateCreated).IsModified = false;
                    entry.Entity.DateModified = DateTime.Now;
                }
            }

        }
    }
}
