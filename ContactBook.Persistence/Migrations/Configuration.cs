namespace ContactBook.Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactBook.Persistence.ContackBookDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ContactBook.Persistence.ContackBookDbContext";
        }

        protected override void Seed(ContactBook.Persistence.ContackBookDbContext context)
        {
            //context.ContactGroups.AddOrUpdate(cg => cg.Name,
            //     new ContactGroup { Name = "Head Office"},
            //     new ContactGroup { Name = "Friends"  }
                 
            // );
            //context.SaveChanges();
            
            base.Seed(context);
        }
    }
}
