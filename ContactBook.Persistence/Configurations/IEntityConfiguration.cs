﻿using System.Data.Entity.ModelConfiguration.Configuration;

namespace ContactBook.Persistence.Configurations
{
    public interface IEntityConfiguration
    {
        void AddConfiguration(ConfigurationRegistrar registrar);
    }
}
