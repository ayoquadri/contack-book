﻿using System.ComponentModel.Composition;
using ContackBook.Model;

namespace ContactBook.Persistence.Configurations
{
    [Export(typeof(IEntityConfiguration))]
    internal class ContactConfiguration : BaseEntityConfiguration<Contact>, IEntityConfiguration
    {
        public ContactConfiguration()
        {

            Property(c => c.Name)
                .IsRequired().HasMaxLength(100);

            Property(c => c.Email)
                .IsRequired();

            Property(c => c.PhoneNumber)
                .IsRequired().HasMaxLength(50);

            Property(c => c.ContactGroup)
                .IsRequired();


        }

    }
}
