﻿using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace ContactBook.Persistence.Configurations
{
    internal class ContextConfiguration
    {
        [ImportMany(typeof(IEntityConfiguration))]
        public IEnumerable<IEntityConfiguration> Configurations { get; set; }
    }
}
