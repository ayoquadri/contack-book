﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ContackBook.Model.Persistence.Repositories
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        Task<TEntity> Get(int id);
        Task Save(TEntity entity);
        Task Update(TEntity entity);
        Task<TEntity> DetachAndUpdate(int id, TEntity modifiedEntity);
        Task<List<TEntity>> Filter(Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> GetAll();
        Task Remove(TEntity entity);
        int Count(Expression<Func<TEntity, bool>> expression);
        int Count();
    }
}
