﻿using System.Linq;

namespace ContackBook.Model.Persistence.Repositories
{
    public interface IContactRepository : IRepository<Contact>
    {
        IQueryable<Contact> GetContacts();
    }
}
