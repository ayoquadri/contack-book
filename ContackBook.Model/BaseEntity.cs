﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ContackBook.Model
{
    public abstract class BaseEntity
    {
       
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }
    }
}
