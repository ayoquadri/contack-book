﻿namespace ContackBook.Model
{
    public class Contact : BaseEntity
    {
        public string Name { get; set; }

        public string PhoneNumber { get; set; }
      
        public string Email { get; set; }

        public string Address { get; set; }

        public ContactGroup ContactGroup { get; set; }

    }
}
