﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ContactBook.Web.App_Start;

namespace ContactBook.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.RegisterMappings();
            //UnityMvcActivator.
        }
    }
}
