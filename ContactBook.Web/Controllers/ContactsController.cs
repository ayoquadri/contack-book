﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using ContackBook.Model;
using ContackBook.Model.Persistence.Repositories;
using ContactBook.Persistence;
using ContactBook.Web.Models;

namespace ContactBook.Web.Controllers
{
    [HandleError]
    public class ContactsController : Controller
    {
        private readonly IContactRepository _contactRepository;
        public ContactsController(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        // GET: Contacts
        public ActionResult Index()
        {
            
            var contacts = _contactRepository.GetAll();

            return View(contacts.ToList());
        }

        // GET: Contacts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var contact = await _contactRepository.Get(id.Value);
            if (contact == null)
            {
                return HttpNotFound();
            }
            var contactModel = Mapper.Map<AddEditContactModel>(contact);

            return View(contactModel);
        }

        // GET: Contacts/Create
        public ActionResult Create()
        {
            //I could use @Html.enumdropdownlistFor on the view too
            ViewBag.ContactGroup = new SelectList(GetEnumSelectList<ContactGroup>(),"Value","Text");
            return View();
        }

        // POST: Contacts/Create
        //Used View Model to guard against overPosting
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AddEditContactModel model)
        {

            if (ModelState.IsValid)
            {
                var contact = Mapper.Map<Contact>(model);
                await _contactRepository.Save(contact);
                TempData["message"] = "Contact Added Successfully";
                return RedirectToAction("Index");
            }

            ViewBag.ContactGroup= new SelectList(GetEnumSelectList<ContactGroup>(), 
                                                        "Value", "Text",model.ContactGroup);
            return View(model);
        }

        // GET: Contacts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var contact = await _contactRepository.Get(id.Value);
            if (contact == null)
            {
                return HttpNotFound();
            }

            var contactModel = Mapper.Map<AddEditContactModel>(contact);

            ViewBag.ContactGroup = new SelectList(GetEnumSelectList<ContactGroup>(), 
                                                            "Value", "Text", contactModel.ContactGroup);

            
            return View(contactModel);
        }

        // POST: Contacts/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AddEditContactModel model)
        {
            if (ModelState.IsValid)
            {
                var contact = Mapper.Map<Contact>(model);
                await _contactRepository.Update(contact);
                TempData["message"] = "Contact Edited Successfully";
                return RedirectToAction("Index");
            }
            ViewBag.ContactGroup = new SelectList(GetEnumSelectList<ContactGroup>(), 
                                                            "Value", "Text",model.ContactGroup);
            return View(model);
        }

        // GET: Contacts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = await _contactRepository.Get(id.Value);
            if (contact == null)
            {
                return HttpNotFound();
            }
            var contactModel = Mapper.Map<AddEditContactModel>(contact);
            return View(contactModel);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Contact contact = await _contactRepository.Get(id);
            if(contact == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            await _contactRepository.Remove(contact);

            TempData["message"] = "Contact Deleted Successfully";
            return RedirectToAction("Index");
        }
        private IEnumerable<SelectListItem> GetEnumSelectList<T>()
        {
            return (Enum.GetValues(typeof(T)).Cast<int>().Select(
                                    e => new SelectListItem()
                                    {
                                        Text = Enum.GetName(typeof(T), e),
                                        Value = e.ToString()

                                    })).ToList();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //_contactRepository.Dispose();
            }
            base.Dispose(disposing);
        }

        
    }
}
