﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ContactBook.Web.Models
{
    public class AddEditContactModel
    {
        public int Id { get; set; }


        [Required]
        [StringLength(100)]
        [RegularExpression(@"^[a-zA-Z0-9]+[a-zA-Z0-9\s-,\.]*",
                        ErrorMessage = "{0} should have an alphabet or number")]
        public string Name { get; set; }

        [DisplayName("Phone Number")]
        [Required]
        [RegularExpression(@"^(?:\+?234|0){1}[7-9]\d[1-9]\d{7}$",
                                ErrorMessage = "{0} is invalid. Enter e.g 07036833337,+2347036833337")]
        public string PhoneNumber { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Please enter a valid email e.g ayo@gmail.com")]
        [StringLength(100)]
        public string Email { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(200)]
        [RegularExpression(@"^[a-zA-Z0-9]+[a-zA-Z0-9\s-,\.]*",
                        ErrorMessage = "{0} should have an alphabet or number")]
        public string Address { get; set; }

        [Required]
        public int? ContactGroup { get; set; }

       // [DisplayName("Contact Group")]
        public string ContactGroupName { get; set; }

    }
}