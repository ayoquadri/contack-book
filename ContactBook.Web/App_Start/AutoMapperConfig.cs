using AutoMapper;
using ContackBook.Model;
using ContactBook.Web.Models;

namespace ContactBook.Web.App_Start
{
    /// <summary>
    /// Specifies the Auto  Mapper configurations
    /// </summary>
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Contact, AddEditContactModel>().
                 ForMember(t => t.ContactGroupName, opt => opt.MapFrom(m => m.ContactGroup.ToString())).ReverseMap();
            }
        );

        }
    }
}
