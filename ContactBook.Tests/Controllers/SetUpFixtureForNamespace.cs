﻿using System.Threading.Tasks;
using ContackBook.Model.Persistence.Repositories;
using ContactBook.Web.App_Start;
using ContactBook.Web.Controllers;
using Moq;
using NUnit.Framework;
using ContactBook.Web.Models;
using TestStack.FluentMVCTesting;

namespace ContactBook.Tests.Controllers
{
    [SetUpFixture]
    public class SetUpFixtureForNamespace
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            AutoMapperConfig.RegisterMappings();
        }
    }
}
