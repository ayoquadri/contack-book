﻿using System;
using System.Net;
using System.Threading.Tasks;
using ContackBook.Model;
using ContackBook.Model.Persistence.Repositories;
using ContactBook.Web.Controllers;
using Moq;
using NUnit.Framework;
using ContactBook.Web.Models;
using TestStack.FluentMVCTesting;

namespace ContactBook.Tests.Controllers
{
    [TestFixture]
    public class ContactControllerTest
    {
        private readonly int TEST_ID = 22;
        private Mock<IContactRepository> mockContactRepository;

        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("SetUp>>>>>>>>>>>>>>>>>");
            mockContactRepository = new Mock<IContactRepository>();
        }

        [Test]
        public void Create_ShouldReturnDefaultView_IfModelIsInvalid()
        {
            var contactController = new ContactsController(mockContactRepository.Object);

           contactController.ModelState.AddModelError("Email","Invalid Email");

            var model = GetModel();

            contactController
                        .WithCallTo(x => x.Create(model)).ShouldRenderDefaultView()
                            .WithModel<AddEditContactModel>();
            
        }

        [Test]
        public void Edit_ShouldRedirectToIndexOnSuccess()
        {
            var contactController = new ContactsController(mockContactRepository.Object);

            var model = GetModel();

            contactController
                        .WithCallTo(x => x.Edit(model))
                                .ShouldRedirectTo(x => x.Index());

            Assert.That(contactController.TempData.ContainsKey("message"), Is.True);
        }

        [Test]
        public void Edit_ShouldReturnNotFound()
        {
            mockContactRepository.Setup(x => x.Get(TEST_ID)).Returns(
                Task.FromResult<Contact>(null));

            var contactController = new ContactsController(mockContactRepository.Object);

            contactController
                .WithCallTo(x => x.Edit(TEST_ID))
                .ShouldGiveHttpStatus(HttpStatusCode.NotFound);
        }

        [Test]
        public void Delete_ShouldReturnNotFound()
        {
            mockContactRepository.Setup(x => x.Get(TEST_ID)).Returns(
                Task.FromResult<Contact>(null));

            var contactController = new ContactsController(mockContactRepository.Object);

            contactController
                .WithCallTo(x => x.Delete(TEST_ID))
                .ShouldGiveHttpStatus(HttpStatusCode.NotFound);
        }

        private AddEditContactModel GetModel()
        {
            return new AddEditContactModel
            {
                Name = "ayodeji",
                ContactGroup = 1,
                PhoneNumber = "+2347036933337",
                Email = "ayo@gmail.com",
                Address = "Lagos, Nigeria"

            };
        }
        
        
    }
   
}
